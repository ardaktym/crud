package kz.aitu.crud.controller;

import kz.aitu.crud.entity.Notification;
import kz.aitu.crud.service.NotificationService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class NotificationCont {
    private final NotificationService notificationService;

    public NotificationCont(NotificationService notificationService) {
        this.notificationService = notificationService;
    }
    @GetMapping(path = "/notification")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(notificationService.getAll());
    }
    @GetMapping(path = "/notification/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id ){
        return ResponseEntity.ok(notificationService.findById(id));
    }
    @DeleteMapping(path = "/notification/delete/{id}")
    public void deleteById(@PathVariable Long id){
        notificationService.deleteById(id);
    }
    @RequestMapping(path = "/notification/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody Notification notification){
        return ResponseEntity.ok(notificationService.updateById(notification));
    }

}
