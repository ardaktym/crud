package kz.aitu.crud.controller;

import kz.aitu.crud.entity.Catalog;
import kz.aitu.crud.service.CatalogService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class CatalogCont {
    private final CatalogService catalogService;

    public CatalogCont(CatalogService catalogService) {
        this.catalogService = catalogService;
    }
    @GetMapping(path = "/catalog")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(catalogService.getAll());
    }
    @GetMapping(path = "/catalog/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(catalogService.findById(id));
    }
    @DeleteMapping(path = "/catalog/delete/{id}")
    public void deleteById(@PathVariable Long id){
        catalogService.deleteById(id);
    }
    @RequestMapping(value = "/catalog/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody Catalog catalog){
        return ResponseEntity.ok(catalogService.updateById(catalog));
    }

}
