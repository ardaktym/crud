package kz.aitu.crud.controller;

import ch.qos.logback.core.pattern.util.RegularEscapeUtil;
import kz.aitu.crud.entity.DestructionAct;
import kz.aitu.crud.service.DestructionActService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class DestructionActCont {
    private final DestructionActService destructionActService;

    public DestructionActCont(DestructionActService destructionActService) {
        this.destructionActService = destructionActService;
    }
    @GetMapping(path = "/destructionAct")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(destructionActService.getAll());
    }
    @GetMapping(path = "/destructionAct/{id}")
    private ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(destructionActService.findById(id));
    }
    @DeleteMapping(path = "/destruction/delete/{id}")
    public void deleteById(@PathVariable Long id){
        destructionActService.deleteById(id);
    }
    @RequestMapping(value = "/destruction/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody DestructionAct destructionAct){
        return ResponseEntity.ok(destructionActService.updateById(destructionAct));
    }
}
