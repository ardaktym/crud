package kz.aitu.crud.controller;

import kz.aitu.crud.entity.Request;
import kz.aitu.crud.service.RequestService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class RequestCont {
    private final RequestService requestService;

    public RequestCont(RequestService requestService) {
        this.requestService = requestService;
    }
    @GetMapping(path = "/request")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(requestService.getAll());
    }
    @GetMapping(path = "/request/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(requestService.findById(id));
    }
    @DeleteMapping(path = "/request/delete/{id}")
    public void deleteById(@PathVariable Long id){
        requestService.deleteById(id);
    }
    @RequestMapping(path = "/request/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody Request request){
        return ResponseEntity.ok(requestService.updateById(request));
    }


}
