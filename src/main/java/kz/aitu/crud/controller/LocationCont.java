package kz.aitu.crud.controller;

import kz.aitu.crud.entity.Location;
import kz.aitu.crud.service.LocationService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

@RestController
public class LocationCont {
    private final LocationService locationService;

    public LocationCont(LocationService locationService) {
        this.locationService = locationService;
    }
    @GetMapping(path = "/location")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(locationService.getAll());
    }
    @GetMapping(path = "/location{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(locationService.findById(id));
    }
    @DeleteMapping(path = "/location/delete/{id}")
    public void deleteById(@PathVariable Long id){
        locationService.deleteById(id);
    }
    @RequestMapping(path = "/location/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody Location location){
        return ResponseEntity.ok(locationService.updateById(location));
    }
}
