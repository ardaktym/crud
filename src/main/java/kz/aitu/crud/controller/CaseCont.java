package kz.aitu.crud.controller;

import kz.aitu.crud.entity.Case;
import kz.aitu.crud.service.CaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class CaseCont {
    private final CaseService caseService;

    public CaseCont(CaseService caseService) {
        this.caseService = caseService;
    }
    @GetMapping(path = "/case")
    public ResponseEntity<?> getAllCase(){
        return ResponseEntity.ok(caseService.getAllCase());
    }
    @GetMapping(path = "/case/{id}")
    public ResponseEntity<?> getById(@PathVariable long id){
        return ResponseEntity.ok(caseService.caseById(id));
    }
    @DeleteMapping(path = "/case/delete/{id}")
    public void deleteById(@PathVariable Long id){
        caseService.deleteById(id);
    }
    @RequestMapping(value = "/case/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody Case casee){
        return ResponseEntity.ok(caseService.updateById(casee));
    }
}
