package kz.aitu.crud.controller;

import kz.aitu.crud.entity.RequestStatusHistory;
import kz.aitu.crud.service.ReqStatHisService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class ReqStatHisCont {
    private final ReqStatHisService reqStatHisService;

    public ReqStatHisCont(ReqStatHisService reqStatHisService) {
        this.reqStatHisService = reqStatHisService;
    }
    @GetMapping(path = "/requestStatusHistory")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(reqStatHisService.getAll());
    }
    @GetMapping(path = "/requestStatusHistory/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(reqStatHisService.findById(id));
    }
    @DeleteMapping(path = "/requestStatusHistory/delete/{id}")
    public void deleteById(@PathVariable Long id){
        reqStatHisService.deleteById(id);
    }
    @RequestMapping(path = "/requestStatusHistory/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody RequestStatusHistory requestStatusHistory){
        return ResponseEntity.ok(reqStatHisService.updateById(requestStatusHistory));
    }
}
