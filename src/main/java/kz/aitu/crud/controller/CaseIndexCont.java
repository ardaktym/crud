package kz.aitu.crud.controller;

import kz.aitu.crud.entity.CaseIndex;
import kz.aitu.crud.service.CaseIndexService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class CaseIndexCont {
    private final CaseIndexService caseIndexService;

    public CaseIndexCont(CaseIndexService caseIndexService) {
        this.caseIndexService = caseIndexService;
    }
    @GetMapping(path = "/caseIndex")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(caseIndexService.getAll());
    }
    @GetMapping(path = "/caseIndex/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(caseIndexService.caseIndexById(id));
    }
    @DeleteMapping(path = "/caseIndex/delete/{id}")
    public void deleteById(@PathVariable Long id){
        caseIndexService.deleteById(id);
    }
    @RequestMapping(value = "/caseIndex/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody CaseIndex caseIndex){
        return ResponseEntity.ok(caseIndexService.updateById(caseIndex));
    }

}
