package kz.aitu.crud.controller;

import kz.aitu.crud.entity.File;
import kz.aitu.crud.service.FileService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class FileCont {
    private final FileService fileService;

    public FileCont(FileService fileService) {
        this.fileService = fileService;
    }
    @GetMapping(path = "/file")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(fileService.getAll());
    }
    @GetMapping(path = "/file/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(fileService.findById(id));
    }
    @DeleteMapping(path = "/file/delete/{id}")
    public void deleteById(@PathVariable Long id){
        fileService.deleteById(id);
    }
    @RequestMapping(value = "/file/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody File file){
        return ResponseEntity.ok(fileService.updateById(file));
    }
}
