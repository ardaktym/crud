package kz.aitu.crud.controller;

import kz.aitu.crud.entity.SearchKey;
import kz.aitu.crud.service.SearchKeyService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchKeyCont {
    private final SearchKeyService searchKeyService;

    public SearchKeyCont(SearchKeyService searchKeyService) {
        this.searchKeyService = searchKeyService;
    }
    @GetMapping(path = "/searchKey")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(searchKeyService.getAll());
    }
    @GetMapping(path = "/searchKey/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(searchKeyService.findById(id));
    }
    @DeleteMapping(path = "/searchKey/delete/{id}")
    public void deleteById(@PathVariable Long id){
        searchKeyService.deleteById(id);
    }
    @RequestMapping(path = "/searchKey/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody SearchKey searchKey)
    {
        return ResponseEntity.ok(searchKeyService.updateById(searchKey));
    }
}
