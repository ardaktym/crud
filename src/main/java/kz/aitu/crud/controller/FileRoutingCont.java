package kz.aitu.crud.controller;

import kz.aitu.crud.entity.FileRouting;
import kz.aitu.crud.service.FileRoutingService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class FileRoutingCont {
    private final FileRoutingService fileRoutingService;

    public FileRoutingCont(FileRoutingService fileRoutingService) {
        this.fileRoutingService = fileRoutingService;
    }
    @GetMapping(path = "/fileRouting")
    private ResponseEntity<?> getAll(){
        return ResponseEntity.ok(fileRoutingService.getAll());
    }
    @GetMapping(path = "/fileRouting/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(fileRoutingService.findById(id));
    }
    @DeleteMapping(path = "/fileRouting/delete/{id}")
    public void deleteById(@PathVariable Long id){
        fileRoutingService.deleteById(id);
    }
    @RequestMapping(path = "/fileRouting/update", method = RequestMethod.PUT)
    private ResponseEntity<?> updateById(@RequestBody FileRouting fileRouting){
        return ResponseEntity.ok(fileRoutingService.updateById(fileRouting));
    }

}
