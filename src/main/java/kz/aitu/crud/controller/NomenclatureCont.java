package kz.aitu.crud.controller;

import kz.aitu.crud.entity.Nomenclature;
import kz.aitu.crud.service.NomenclatureService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class NomenclatureCont {
    private final NomenclatureService nomenclatureService;

    public NomenclatureCont(NomenclatureService nomenclatureService) {
        this.nomenclatureService = nomenclatureService;
    }
    @GetMapping(path = "/nomenclature")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(nomenclatureService.getAll());
    }
    @GetMapping(path = "/nomenclature/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(nomenclatureService.findById(id));
    }
    @DeleteMapping(path = "/nomenclature/delete{id}")
    public void deleteById(@PathVariable Long id){
        nomenclatureService.deleteById(id);
    }
    @RequestMapping(path = "/nomenclature/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody Nomenclature nomenclature){
        return ResponseEntity.ok(nomenclatureService.updateById(nomenclature));
    }
}
