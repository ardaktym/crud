package kz.aitu.crud.controller;

import kz.aitu.crud.entity.SearchKeyRouting;
import kz.aitu.crud.service.SearchKeyRoutingSer;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class SearchKeyRoutingCont {
    private final SearchKeyRoutingSer searchKeyRoutingSer;

    public SearchKeyRoutingCont(SearchKeyRoutingSer searchKeyRoutingSer) {
        this.searchKeyRoutingSer = searchKeyRoutingSer;
    }
    @GetMapping(path = "/searchKeyRouting")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(searchKeyRoutingSer.getAll());
    }
    @GetMapping(path = "/searchKeyRouting/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(searchKeyRoutingSer.findById(id));
    }
    @DeleteMapping(path = "/searchKeyRouting/delete/{id}")
    public void deleteById(@PathVariable Long id){
        searchKeyRoutingSer.deleteById(id);
    }
    @RequestMapping(path = "/searchKeyRouting/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody SearchKeyRouting searchKeyRouting){
        return ResponseEntity.ok(searchKeyRoutingSer.updateById(searchKeyRouting));
    }

}
