package kz.aitu.crud.controller;

import kz.aitu.crud.entity.Company;
import kz.aitu.crud.service.CompanyService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyCont {
    private final CompanyService companyService;

    public CompanyCont(CompanyService companyService) {
        this.companyService = companyService;
    }
    @GetMapping(path = "/company")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(companyService.getAll());
    }
    @GetMapping(path = "/company/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(findById(id));
    }
    @DeleteMapping(path = "/company/delete/{id}")
    public void deleteById(@PathVariable Long id){
        companyService.deleteById(id);
    }
    @RequestMapping(value = "/company/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody Company company){
        return ResponseEntity.ok(companyService.updateById(company));
    }



}
