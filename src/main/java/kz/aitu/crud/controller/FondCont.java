package kz.aitu.crud.controller;

import kz.aitu.crud.entity.Fond;
import kz.aitu.crud.service.FondService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class FondCont {
    private final FondService fondService;

    public FondCont(FondService fondService) {
        this.fondService = fondService;
    }
    @GetMapping(path = "/fond")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(fondService.getAll());
    }
    @GetMapping(path = "/fond/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(fondService.findById(id));
    }
    @DeleteMapping(path = "/fond/delete/{id}")
    public void deleteById(@PathVariable Long id){
        fondService.deleteById(id);
    }
    @RequestMapping(path = "/fond/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody Fond fond){
        return ResponseEntity.ok(fondService.updateById(fond));
    }

}

