package kz.aitu.crud.controller;

import kz.aitu.crud.entity.Auth;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import kz.aitu.crud.service.AuthService;

@RestController

public class AuthCont {
    private final AuthService authService;
    public AuthCont(AuthService authService) {
        this.authService = authService;
    }
    @GetMapping(path="/authorization")
    public ResponseEntity<?> getAllAuthorization(){
        return ResponseEntity.ok(authService.getAllAuthorization());
    }
    @GetMapping(path = "/authorization/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id){
        return ResponseEntity.ok(authService.authById(id));
    }
    @DeleteMapping(path = "/authorization/delete/{id}")
    public void deleteById(@PathVariable Long id){
        authService.deleteById(id);
    }
    @RequestMapping(value = "/authorization/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody Auth auth){
        return ResponseEntity.ok(authService.updateById(auth));
    }




}
