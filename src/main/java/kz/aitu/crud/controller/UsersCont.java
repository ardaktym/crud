package kz.aitu.crud.controller;

import kz.aitu.crud.entity.Users;
import kz.aitu.crud.service.UsersService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController

public class UsersCont {
    private final UsersService usersService;

    public UsersCont(UsersService usersService) {
        this.usersService = usersService;
    }
    @GetMapping(path = "/users")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(usersService.getAll());
    }
    @GetMapping(path = "/users/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(usersService.findById(id));
    }
    @DeleteMapping(path = "/users/delete/{id}")
    public void deleteById(@PathVariable Long id){
        usersService.deleteById(id);
    }
    @RequestMapping(value = "/users/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody Users users){
        return ResponseEntity.ok(usersService.updateById(users));
    }

}
