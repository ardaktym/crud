package kz.aitu.crud.controller;

import kz.aitu.crud.entity.Record1;
import kz.aitu.crud.service.RecordService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class RecordCont {
    private final RecordService recordService;

    public RecordCont(RecordService recordService) {
        this.recordService = recordService;
    }
    @GetMapping(path = "/record")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(recordService.getAll());
    }
    @GetMapping(path = "/record/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(recordService.findById(id));
    }
    @DeleteMapping(path = "/record/delelte/{id}")
    public void deleteById(@PathVariable Long id){
        recordService.deleteById(id);
    }
    @RequestMapping(path = "/record/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody Record1 record){
        return ResponseEntity.ok(recordService.updateById(record));
    }
}
