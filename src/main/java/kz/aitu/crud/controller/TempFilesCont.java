package kz.aitu.crud.controller;

import kz.aitu.crud.entity.TempFiles;
import kz.aitu.crud.service.TempFilesService;
import lombok.experimental.PackagePrivate;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class TempFilesCont {
    private final TempFilesService tempFilesService;

    public TempFilesCont(TempFilesService tempFilesService) {
        this.tempFilesService = tempFilesService;
    }
    @GetMapping(path = "/tempFiles")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(tempFilesService.getAll());
    }
    @GetMapping(path = "/tempFiles/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(tempFilesService.findById(id));
    }
    @DeleteMapping(path = "/tempFiles/delete/{id}")
    public void deleteById(@PathVariable Long id){
        tempFilesService.deleteById(id);
    }
    @RequestMapping(path = "/tempFiles/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody TempFiles tempFiles){
        return ResponseEntity.ok(tempFilesService.updateById(tempFiles));
    }
}
