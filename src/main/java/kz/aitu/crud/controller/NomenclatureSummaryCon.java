package kz.aitu.crud.controller;

import kz.aitu.crud.entity.NomenclatureSummary;
import kz.aitu.crud.service.NomenclatureSummaryService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class NomenclatureSummaryCon {
    private final NomenclatureSummaryService nomenclatureSummaryService;

    public NomenclatureSummaryCon(NomenclatureSummaryService nomenclatureSummaryService) {
        this.nomenclatureSummaryService = nomenclatureSummaryService;
    }
    @GetMapping(path = "/nomenclatureSummary")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(nomenclatureSummaryService.getAll());
    }
    @GetMapping(path = "/nomenclatureSummary/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(nomenclatureSummaryService.findById(id));
    }
    @DeleteMapping(path = "/nomenclatureSummary/delete/{id}")
    public void deleteById(@PathVariable Long id){
        nomenclatureSummaryService.deleteById(id);
    }
    @RequestMapping(path = "/nomenclatureSummary/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody NomenclatureSummary nomenclatureSummary){
        return ResponseEntity.ok(nomenclatureSummaryService.updateById(nomenclatureSummary));
    }
}
