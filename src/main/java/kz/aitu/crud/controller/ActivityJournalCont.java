package kz.aitu.crud.controller;


import kz.aitu.crud.entity.ActivityJournal;
import kz.aitu.crud.service.ActivityJournalService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class ActivityJournalCont {
    private final ActivityJournalService activityJournalService;
    public ActivityJournalCont(ActivityJournalService activityJournalService)
    {
        this.activityJournalService = activityJournalService;
    }
    @GetMapping(path = "/activityJournal")
    public ResponseEntity<?> getAllActivityJournal(){
        return ResponseEntity.ok(activityJournalService.getAllActivityJournal());
    }
    @GetMapping(path = "/activityJournal/{id}")
    public ResponseEntity<?> getActivityJournalById(@PathVariable Long id){
        return ResponseEntity.ok(activityJournalService.activityJournalById(id));
    }
    @DeleteMapping(path = "/activityJournal/delete/{id}")
    public void deleteActivityJournalById(@PathVariable Long id){
        activityJournalService.deleteById(id);
    }
    @RequestMapping(value = "/activityJournal/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateActivityJournalById(@RequestBody ActivityJournal activityJournal){
        return ResponseEntity.ok(activityJournalService.updateById(activityJournal));
    }

}
