package kz.aitu.crud.controller;

import kz.aitu.crud.entity.Share;
import kz.aitu.crud.service.ShareService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class ShareCont {
    private final ShareService shareService;

    public ShareCont(ShareService shareService) {
        this.shareService = shareService;
    }
    @GetMapping(path = "/share")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(shareService.getAll());
    }
    @GetMapping(path = "/share/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(shareService.findById(id));
    }
    @DeleteMapping(path = "/share/delete/{id}")
    public void deleteById(@PathVariable Long id){
        shareService.deleteById(id);
    }
    @RequestMapping(path = "/share/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody Share share){
        return ResponseEntity.ok(shareService.updateById(share));
    }
}
