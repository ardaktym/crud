package kz.aitu.crud.controller;

import kz.aitu.crud.entity.CompanyUnit;
import kz.aitu.crud.service.CompanyUnitService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class CompanyUnitCont {
    private final CompanyUnitService companyUnitService;

    public CompanyUnitCont(CompanyUnitService companyUnitService) {
        this.companyUnitService = companyUnitService;
    }
    @GetMapping(path = "/companyUnit")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(companyUnitService.getAll());
    }
    @GetMapping(path = "/companyUnit/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(companyUnitService.findById(id));
    }
    @DeleteMapping(path = "/companyUnit/delete/{id}")
    public void deleteById(@PathVariable Long id){
        companyUnitService.deleteById(id);
    }
    @RequestMapping(value = "/companyUnit/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody CompanyUnit companyUnit){
        return ResponseEntity.ok(companyUnitService.updateById(companyUnit));
    }
}
