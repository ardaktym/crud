package kz.aitu.crud.controller;

import kz.aitu.crud.entity.CatalogCase;
import kz.aitu.crud.service.CatalogCaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class CatalogCaseCont {
    private final CatalogCaseService catalogCaseService;

    public CatalogCaseCont(CatalogCaseService catalogCaseService) {
        this.catalogCaseService = catalogCaseService;
    }
    @GetMapping(path = "/catalogCase")
    public ResponseEntity<?> getAll(){
        return ResponseEntity.ok(catalogCaseService.getAll());
    }
    @GetMapping(path = "/catalogCase/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id){
        return ResponseEntity.ok(catalogCaseService.findById(id));
    }
    @DeleteMapping(path = "/catalogCase/delete/{id}")
    public void deleteById(@PathVariable Long id){
        catalogCaseService.deleteById(id);
    }
    @RequestMapping(value = "/catalogCase/update", method = RequestMethod.PUT)
    public ResponseEntity<?> updateById(@RequestBody CatalogCase catalogCase){
        return ResponseEntity.ok(catalogCaseService.updateById(catalogCase));
    }

}
