package kz.aitu.crud.service;

import kz.aitu.crud.entity.ActivityJournal;
import kz.aitu.crud.repository.ActivityJournalRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;


@Service
public class ActivityJournalService {
    ActivityJournalRep activityJournalRep;
    public ActivityJournalService(ActivityJournalRep activityJournalRep) {
        this.activityJournalRep = activityJournalRep;
    }
    public List<ActivityJournal> getAllActivityJournal(){
        return (List<ActivityJournal>) activityJournalRep.findAll();
    }
    public Optional<ActivityJournal> activityJournalById(Long id){
        return activityJournalRep.findById(id);
    }
    public void deleteById(Long id){
        activityJournalRep.deleteById(id);;
    }
    public ActivityJournal updateById(@RequestBody ActivityJournal activityJournal){
        return activityJournalRep.save(activityJournal);
    }
}
