package kz.aitu.crud.service;

import kz.aitu.crud.entity.Record1;
import kz.aitu.crud.repository.RecordRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class RecordService {
    RecordRep recordRep;

    public RecordService(RecordRep recordRep) {
        this.recordRep = recordRep;
    }
    public List<Record1> getAll(){
        return (List<Record1>) recordRep.findAll();
    }
    public Optional<Record1> findById(Long id){
        return recordRep.findById(id);
    }
    public void deleteById(Long id){
        recordRep.deleteById(id);
    }
    public Record1 updateById(@RequestBody Record1 record){
        return recordRep.save(record);
    }


}
