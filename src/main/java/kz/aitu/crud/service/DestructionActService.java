package kz.aitu.crud.service;

import kz.aitu.crud.entity.DestructionAct;
import kz.aitu.crud.repository.DestructionActRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class DestructionActService {
    DestructionActRep destructionActRep;

    public DestructionActService(DestructionActRep destructionActRep) {
        this.destructionActRep = destructionActRep;
    }
    public List<DestructionAct> getAll(){
        return (List<DestructionAct>) destructionActRep.findAll();
    }
    public Optional<DestructionAct> findById(Long id){
        return destructionActRep.findById(id);
    }
    public void deleteById(Long id){
        destructionActRep.deleteById(id);
    }
    public DestructionAct updateById(@RequestBody DestructionAct destructionAct){
        return destructionActRep.save(destructionAct);
    }
}
