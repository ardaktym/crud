package kz.aitu.crud.service;

import kz.aitu.crud.entity.RequestStatusHistory;
import kz.aitu.crud.repository.RequestStatusHistoryRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class ReqStatHisService {
    RequestStatusHistoryRep requestStatusHistoryRep;

    public ReqStatHisService(RequestStatusHistoryRep requestStatusHistoryRep) {
        this.requestStatusHistoryRep = requestStatusHistoryRep;
    }
    public List<RequestStatusHistory> getAll(){
        return (List<RequestStatusHistory>) requestStatusHistoryRep.findAll();
    }
    public Optional<RequestStatusHistory> findById(Long id){
        return requestStatusHistoryRep.findById(id);
    }
    public void deleteById(Long id){
        requestStatusHistoryRep.deleteById(id);
    }
    public RequestStatusHistory updateById(@RequestBody RequestStatusHistory requestStatusHistory) {
        return requestStatusHistoryRep.save(requestStatusHistory);
    }
}
