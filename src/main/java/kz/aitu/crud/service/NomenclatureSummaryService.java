package kz.aitu.crud.service;

import kz.aitu.crud.entity.NomenclatureSummary;
import kz.aitu.crud.repository.NomenclatureSummaryRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class NomenclatureSummaryService {
    NomenclatureSummaryRep nomenclatureSummaryRep;

    public NomenclatureSummaryService(NomenclatureSummaryRep nomenclatureSummaryRep) {
        this.nomenclatureSummaryRep = nomenclatureSummaryRep;
    }
    public List<NomenclatureSummary> getAll(){
        return (List<NomenclatureSummary>) nomenclatureSummaryRep.findAll();
    }
    public Optional<NomenclatureSummary> findById(Long id){
        return nomenclatureSummaryRep.findById(id);
    }
    public void deleteById(Long id){
        nomenclatureSummaryRep.deleteById(id);
    }
    public NomenclatureSummary updateById(@RequestBody NomenclatureSummary nomenclatureSummary){
        return nomenclatureSummaryRep.save(nomenclatureSummary);
    }
}
