package kz.aitu.crud.service;

import kz.aitu.crud.entity.File;
import kz.aitu.crud.entity.FileRouting;
import kz.aitu.crud.repository.FileRoutingRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class FileRoutingService {
    FileRoutingRep fileRoutingRep;

    public FileRoutingService(FileRoutingRep fileRoutingRep) {
        this.fileRoutingRep = fileRoutingRep;
    }
    public List<FileRouting> getAll(){
        return (List<FileRouting>) fileRoutingRep.findAll();
    }
    public Optional<FileRouting> findById(Long id){
        return fileRoutingRep.findById(id);
    }
    public void deleteById(Long id){
        fileRoutingRep.deleteById(id);
    }
    public FileRouting updateById(@RequestBody FileRouting fileRouting){
        return fileRoutingRep.save(fileRouting);
    }


}
