package kz.aitu.crud.service;

import kz.aitu.crud.entity.CaseIndex;
import kz.aitu.crud.repository.CaseIndexRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class CaseIndexService {
    CaseIndexRep caseIndexRep;

    public CaseIndexService(CaseIndexRep caseIndexRep) {
        this.caseIndexRep = caseIndexRep;
    }
    public List<CaseIndex> getAll(){
        return (List<CaseIndex>) caseIndexRep.findAll();
    }
    public Optional<CaseIndex> caseIndexById(Long id){
        return caseIndexRep.findById(id);
    }
    public void deleteById(Long id){
        caseIndexRep.deleteById(id);
    }
    public CaseIndex updateById(@RequestBody CaseIndex caseIndex){
        return caseIndexRep.save(caseIndex);
    }

}
