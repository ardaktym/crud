package kz.aitu.crud.service;

import kz.aitu.crud.entity.Auth;
import kz.aitu.crud.repository.AuthRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class AuthService {
    AuthRep authRep;

    public AuthService(AuthRep authRep) {
        this.authRep = authRep;
    }

    public List<Auth> getAllAuthorization(){
        return (List<Auth>) authRep.findAll();
    }

    public Optional<Auth> authById(Long id){
        return authRep.findById(id);
    }
    public void deleteById(Long id){
        authRep.deleteById(id);
    }
    public Auth updateById(@RequestBody Auth auth){
        return authRep.save(auth);
    }



}
