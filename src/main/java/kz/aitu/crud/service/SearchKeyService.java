package kz.aitu.crud.service;

import kz.aitu.crud.entity.SearchKey;
import kz.aitu.crud.repository.SearchKeyRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class SearchKeyService {
    SearchKeyRep searchKeyRep;

    public SearchKeyService(SearchKeyRep searchKeyRep) {
        this.searchKeyRep = searchKeyRep;
    }
    public List<SearchKey> getAll(){
        return (List<SearchKey>) searchKeyRep.findAll();
    }
    public Optional<SearchKey> findById(Long id){
        return searchKeyRep.findById(id);
    }
    public void deleteById(Long id){
        searchKeyRep.deleteById(id);
    }
    public SearchKey updateById(@RequestBody SearchKey searchKey){
        return searchKeyRep.save(searchKey);
    }
}
