package kz.aitu.crud.service;

import kz.aitu.crud.entity.CatalogCase;
import kz.aitu.crud.repository.CatalogCaseRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class CatalogCaseService {
    CatalogCaseRep catalogCaseRep;

    public CatalogCaseService(CatalogCaseRep catalogCaseRep) {
        this.catalogCaseRep = catalogCaseRep;
    }
    public List<CatalogCase> getAll(){
        return (List<CatalogCase>) catalogCaseRep.findAll();
    }
    public Optional<CatalogCase> findById(Long id){
        return catalogCaseRep.findById(id);
    }
    public void deleteById(Long id){
        catalogCaseRep.deleteById(id);
    }
    public CatalogCase updateById(@RequestBody CatalogCase catalogCase){
        return catalogCaseRep.save(catalogCase);
    }

}
