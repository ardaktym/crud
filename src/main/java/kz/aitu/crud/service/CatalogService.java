package kz.aitu.crud.service;

import kz.aitu.crud.entity.Catalog;
import kz.aitu.crud.repository.CatalogRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;


import java.util.List;
import java.util.Optional;

@Service
public class CatalogService {
    CatalogRep catalogRep;

    public CatalogService(CatalogRep catalogRep) {
        this.catalogRep = catalogRep;
    }
    public List<Catalog> getAll(){
        return (List<Catalog>) catalogRep.findAll();
    }
    public Optional<Catalog> findById(Long id){
        return catalogRep.findById(id);
    }
    public void deleteById(Long id){
        catalogRep.deleteById(id);
    }
    public Catalog updateById(@RequestBody Catalog catalog){
        return catalogRep.save(catalog);
    }
}
