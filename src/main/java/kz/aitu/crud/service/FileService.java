package kz.aitu.crud.service;

import kz.aitu.crud.entity.File;
import kz.aitu.crud.repository.FileRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class FileService {
    FileRep fileRep;

    public FileService(FileRep fileRep) {
        this.fileRep = fileRep;
    }
    public List<File> getAll(){
        return (List<File>) fileRep.findAll();
    }
    public Optional<File> findById(Long id){
        return fileRep.findById(id);
    }
    public void deleteById(Long id){
        fileRep.deleteById(id);
    }
    public File updateById(@RequestBody File file){
        return fileRep.save(file);
    }

}
