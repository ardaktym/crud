package kz.aitu.crud.service;

import kz.aitu.crud.entity.Notification;
import kz.aitu.crud.repository.NotificationRep;
import org.aspectj.weaver.ast.Not;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;

@Service
public class NotificationService {
    NotificationRep notificationRep;

    public NotificationService(NotificationRep notificationRep) {
        this.notificationRep = notificationRep;
    }
    public List<Notification> getAll(){
        return (List<Notification>) notificationRep.findAll();
    }
    public Optional<Notification> findById(Long id){
        return notificationRep.findById(id);
    }
    public void deleteById(Long id){
        notificationRep.deleteById(id);
    }
    public Notification updateById(@RequestBody Notification notification){
        return notificationRep.save(notification);
    }
}
