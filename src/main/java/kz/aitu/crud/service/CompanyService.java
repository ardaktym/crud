package kz.aitu.crud.service;

import kz.aitu.crud.entity.Company;
import kz.aitu.crud.repository.CompanyRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service

public class CompanyService {
    CompanyRep companyRep;

    public CompanyService(CompanyRep companyRep) {
        this.companyRep = companyRep;
    }
    public List<Company> getAll(){
        return (List<Company>) companyRep.findAll();
    }
    public Optional<Company> findById(Long id){
        return companyRep.findById(id);
    }
    public void deleteById(Long id){
        companyRep.deleteById(id);
    }
    public Company updateById(@RequestBody Company company){
        return companyRep.save(company);
    }
}
