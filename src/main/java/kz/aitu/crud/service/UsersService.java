package kz.aitu.crud.service;

import kz.aitu.crud.entity.Users;
import kz.aitu.crud.repository.UsersRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class UsersService {
    UsersRep usersRep;

    public UsersService(UsersRep usersRep) {
        this.usersRep = usersRep;
    }
    public List<Users> getAll(){
        return (List<Users>) usersRep.findAll();
    }
    public Optional<Users> findById(Long id){
        return usersRep.findById(id);
    }
    public void deleteById(Long id){
        usersRep.deleteById(id);
    }
    public Users updateById(@RequestBody Users users){
        return usersRep.save(users);
    }
}
