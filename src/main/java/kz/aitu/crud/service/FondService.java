package kz.aitu.crud.service;

import kz.aitu.crud.entity.Fond;
import kz.aitu.crud.repository.FondRep;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class FondService {
    FondRep fondRep;

    public FondService(FondRep fondRep) {
        this.fondRep = fondRep;
    }
    public List<Fond> getAll(){
        return (List<Fond>) fondRep.findAll();
    }
    public Optional<Fond> findById(Long id){
        return fondRep.findById(id);
    }
    public void deleteById(Long id){
        fondRep.deleteById(id);
    }
    public Fond updateById(@RequestBody Fond fond){
        return fondRep.save(fond);
    }
}
