package kz.aitu.crud.service;

import kz.aitu.crud.entity.Share;
import kz.aitu.crud.repository.ShareRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class ShareService {
    ShareRep shareRep;

    public ShareService(ShareRep shareRep) {
        this.shareRep = shareRep;
    }
    public List<Share> getAll(){
        return (List<Share>) shareRep.findAll();
    }
    public Optional<Share> findById(Long id){
        return shareRep.findById(id);
    }
    public void deleteById(Long id){
        shareRep.deleteById(id);
    }
    public Share updateById(@RequestBody Share share){
        return shareRep.save(share);
    }
}
