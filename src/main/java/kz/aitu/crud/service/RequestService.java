package kz.aitu.crud.service;

import kz.aitu.crud.entity.Request;
import kz.aitu.crud.repository.RequestRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class RequestService {
    RequestRep requestRep;

    public RequestService(RequestRep requestRep) {
        this.requestRep = requestRep;
    }
    public List<Request> getAll(){
        return (List<Request>) requestRep.findAll();
    }
    public Optional<Request> findById(Long id){
        return requestRep.findById(id);
    }
    public void deleteById(Long id){
        requestRep.deleteById(id);
    }
    public Request updateById(@RequestBody Request request){
        return requestRep.save(request);
    }
}
