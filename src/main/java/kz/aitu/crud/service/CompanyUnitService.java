package kz.aitu.crud.service;

import kz.aitu.crud.entity.CompanyUnit;
import kz.aitu.crud.repository.CompanyUnitRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyUnitService {
    CompanyUnitRep companyUnitRep;

    public CompanyUnitService(CompanyUnitRep companyUnitRep) {
        this.companyUnitRep = companyUnitRep;
    }
    public List<CompanyUnit> getAll(){
        return (List<CompanyUnit>) companyUnitRep.findAll();
    }
    public Optional<CompanyUnit> findById(Long id){
        return companyUnitRep.findById(id);
    }
    public void deleteById(Long id){
        companyUnitRep.deleteById(id);
    }
    public CompanyUnit updateById(@RequestBody CompanyUnit companyUnit){
        return companyUnitRep.save(companyUnit);
    }



}
