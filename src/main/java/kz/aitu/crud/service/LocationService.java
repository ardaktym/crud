package kz.aitu.crud.service;

import kz.aitu.crud.entity.Location;
import kz.aitu.crud.repository.LocationRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class LocationService {
    LocationRep locationRep;

    public LocationService(LocationRep locationRep) {
        this.locationRep = locationRep;
    }
    public List<Location> getAll(){
        return (List<Location>) locationRep.findAll();
    }
    public Optional<Location> findById(Long id){
        return locationRep.findById(id);
    }
    public void deleteById(Long id){
        locationRep.deleteById(id);
    }
    public Location updateById(@RequestBody Location location){
        return locationRep.save(location);
    }
}
