package kz.aitu.crud.service;

import kz.aitu.crud.entity.TempFiles;
import kz.aitu.crud.repository.TempFilesRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class TempFilesService {
    TempFilesRep tempFilesRep;

    public TempFilesService(TempFilesRep tempFilesRep) {
        this.tempFilesRep = tempFilesRep;
    }
    public List<TempFiles> getAll(){
        return (List<TempFiles>) tempFilesRep.findAll();
    }
    public Optional<TempFiles> findById(Long id){
        return tempFilesRep.findById(id);
    }
    public void deleteById(Long id){
        tempFilesRep.deleteById(id);
    }
    public TempFiles updateById(@RequestBody TempFiles tempFiles){
        return tempFilesRep.save(tempFiles);
    }

}
