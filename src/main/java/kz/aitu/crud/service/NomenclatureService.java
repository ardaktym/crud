package kz.aitu.crud.service;

import kz.aitu.crud.entity.Nomenclature;
import kz.aitu.crud.repository.NomenclatureRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class NomenclatureService {
    NomenclatureRep nomenclatureRep;

    public NomenclatureService(NomenclatureRep nomenclatureRep) {
        this.nomenclatureRep = nomenclatureRep;
    }
    public List<Nomenclature> getAll(){
        return (List<Nomenclature>) nomenclatureRep.findAll();
    }
    public Optional<Nomenclature> findById(Long id){
        return nomenclatureRep.findById(id);
    }
    public void deleteById(Long id){
        nomenclatureRep.deleteById(id);
    }
    public Nomenclature updateById(@RequestBody Nomenclature nomenclature){
        return nomenclatureRep.save(nomenclature);
    }

}
