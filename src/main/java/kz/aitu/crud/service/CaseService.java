package kz.aitu.crud.service;

import kz.aitu.crud.entity.Case;
import kz.aitu.crud.repository.CaseRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;

@Service
public class CaseService {
    CaseRep caseRep;

    public CaseService(CaseRep caseRep) {
        this.caseRep = caseRep;
    }

    public List<Case> getAllCase(){
        return (List<Case>) caseRep.findAll();
    }

    public Optional<Case> caseById(Long id){
        return caseRep.findById(id);
    }
    public void deleteById(Long id){
        caseRep.deleteById(id);
    }
    public Case updateById(@RequestBody Case cases ){
        return caseRep.save(cases);
    }


}
