package kz.aitu.crud.service;

import kz.aitu.crud.entity.SearchKeyRouting;
import kz.aitu.crud.repository.SearchKeyRoutingRep;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Service
public class SearchKeyRoutingSer {
    SearchKeyRoutingRep searchKeyRoutingRep;

    public SearchKeyRoutingSer(SearchKeyRoutingRep searchKeyRoutingRep) {
        this.searchKeyRoutingRep = searchKeyRoutingRep;
    }
    public List<SearchKeyRouting> getAll(){
        return (List<SearchKeyRouting>) searchKeyRoutingRep.findAll();
    }
    public Optional<SearchKeyRouting> findById(Long id){
        return searchKeyRoutingRep.findById(id);
    }
    public void deleteById(Long id){
        searchKeyRoutingRep.deleteById(id);
    }
    public SearchKeyRouting updateById(@RequestBody SearchKeyRouting searchKeyRouting){
        return searchKeyRoutingRep.save(searchKeyRouting);
    }

}
