package kz.aitu.crud.repository;

import kz.aitu.crud.entity.FileRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRoutingRep extends CrudRepository<FileRouting, Long> {
}
