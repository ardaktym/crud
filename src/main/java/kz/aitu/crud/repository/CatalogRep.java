package kz.aitu.crud.repository;

import kz.aitu.crud.entity.Catalog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CatalogRep extends CrudRepository<Catalog, Long> {
}
