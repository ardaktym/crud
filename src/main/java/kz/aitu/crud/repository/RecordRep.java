package kz.aitu.crud.repository;

import kz.aitu.crud.entity.Record1;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecordRep extends CrudRepository<Record1, Long> {
}
