package kz.aitu.crud.repository;

import kz.aitu.crud.entity.RequestStatusHistory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface RequestStatusHistoryRep extends CrudRepository<RequestStatusHistory, Long> {

}
