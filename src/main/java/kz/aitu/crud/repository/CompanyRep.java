package kz.aitu.crud.repository;

import kz.aitu.crud.entity.Company;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRep extends CrudRepository<Company, Long> {
}
