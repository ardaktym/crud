package kz.aitu.crud.repository;

import kz.aitu.crud.entity.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRep extends CrudRepository<Users, Long> {
}
