package kz.aitu.crud.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import kz.aitu.crud.entity.Auth;

@Repository

public interface AuthRep extends CrudRepository<Auth, Long> {

}
