package kz.aitu.crud.repository;

import kz.aitu.crud.entity.Nomenclature;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NomenclatureRep extends CrudRepository<Nomenclature, Long> {

}
