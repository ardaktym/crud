package kz.aitu.crud.repository;

import kz.aitu.crud.entity.SearchKey;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchKeyRep extends CrudRepository<SearchKey, Long> {
}
