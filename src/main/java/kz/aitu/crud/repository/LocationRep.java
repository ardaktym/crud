package kz.aitu.crud.repository;

import kz.aitu.crud.entity.Location;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationRep extends CrudRepository<Location, Long> {
}
