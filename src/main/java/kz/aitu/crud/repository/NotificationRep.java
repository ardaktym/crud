package kz.aitu.crud.repository;

import kz.aitu.crud.entity.Notification;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationRep extends CrudRepository<Notification, Long> {
}
