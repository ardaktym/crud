package kz.aitu.crud.repository;

import kz.aitu.crud.entity.CompanyUnit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyUnitRep extends CrudRepository<CompanyUnit, Long> {
}
