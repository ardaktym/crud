package kz.aitu.crud.repository;

import kz.aitu.crud.entity.NomenclatureSummary;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NomenclatureSummaryRep extends CrudRepository<NomenclatureSummary, Long> {
}
