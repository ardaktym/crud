package kz.aitu.crud.repository;

import kz.aitu.crud.entity.ActivityJournal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface ActivityJournalRep extends CrudRepository<ActivityJournal, Long> {
}
