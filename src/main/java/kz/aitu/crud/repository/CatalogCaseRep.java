package kz.aitu.crud.repository;

import kz.aitu.crud.entity.CatalogCase;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CatalogCaseRep extends CrudRepository<CatalogCase, Long> {
}
