package kz.aitu.crud.repository;

import kz.aitu.crud.entity.File;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileRep extends CrudRepository<File, Long> {
}
