package kz.aitu.crud.repository;

import kz.aitu.crud.entity.SearchKeyRouting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SearchKeyRoutingRep extends CrudRepository<SearchKeyRouting, Long> {
}
