package kz.aitu.crud.repository;

import kz.aitu.crud.entity.TempFiles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TempFilesRep extends CrudRepository<TempFiles, Long> {
}
