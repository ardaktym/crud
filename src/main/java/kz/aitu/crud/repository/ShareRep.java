package kz.aitu.crud.repository;

import kz.aitu.crud.entity.Share;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShareRep extends CrudRepository<Share, Long> {
}
