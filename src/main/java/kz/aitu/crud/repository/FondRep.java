package kz.aitu.crud.repository;

import kz.aitu.crud.entity.Fond;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FondRep extends CrudRepository<Fond, Long> {
}
