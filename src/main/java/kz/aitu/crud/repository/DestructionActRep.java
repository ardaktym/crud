package kz.aitu.crud.repository;

import kz.aitu.crud.entity.DestructionAct;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DestructionActRep extends CrudRepository<DestructionAct, Long> {
}
