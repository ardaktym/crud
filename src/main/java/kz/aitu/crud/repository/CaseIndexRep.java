package kz.aitu.crud.repository;

import kz.aitu.crud.entity.CaseIndex;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CaseIndexRep extends CrudRepository<CaseIndex, Long> {
}
