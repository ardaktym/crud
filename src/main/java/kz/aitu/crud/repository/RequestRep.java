package kz.aitu.crud.repository;

import kz.aitu.crud.entity.Request;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestRep extends CrudRepository<Request, Long> {
}
