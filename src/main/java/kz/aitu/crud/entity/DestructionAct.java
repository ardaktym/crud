package kz.aitu.crud.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "destruction_act")
public class DestructionAct {
    @Id
    private long id;
    @Column
    private String act_number;
    private String footing;
    private long id_structural_subdivision;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;
}
