package kz.aitu.crud.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "company_unit")
public class CompanyUnit {
    @Id
    private long id;
    @Column
    private String name_ru;
    private String name_kz;
    private String name_en;
    private long parent_id;
    private long year;
    private long company_id;
    private String company_index;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;

}
