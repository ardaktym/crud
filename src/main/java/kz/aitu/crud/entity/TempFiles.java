package kz.aitu.crud.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "temp_files")
public class TempFiles {
    @Id
    private long id;
    @Column
    private String file_binary;
    private short file_binary_byte;
}
