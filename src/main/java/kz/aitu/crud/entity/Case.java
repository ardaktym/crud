package kz.aitu.crud.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "case_")
public class Case {
    @Id
    private long id;
    @Column
    private String case_number;
    private String case_tome;
    private String title_ru;
    private String title_kz;
    private String title_en;
    private long start_date;
    private long finished_date;
    private long page_number;
    private boolean eds_signature_flag;
    private String eds_signature;
    private boolean sending_naf_flag;
    private boolean deletion_flag;
    private boolean limited_access_flag;
    private String hash;
    private int version;
    private String id_version;
    private boolean active_version_flag;
    private String note;
    private long id_location;
    private long id_case_index;
    private long id_inventory;
    private long id_destruction_act;
    private long id_structural_subdivision;
    private String blockchain_address;
    private long blockchain_add_date;
    private long created_timestamp;
    private long created_by;
    private long updated_timestamp;
    private long updated_by;

}
