drop table if exists activity_journal CASCADE;
CREATE TABLE activity_journal (
                                  id BIGSERIAL NOT NULL,
                                  even_type VARCHAR(128),
                                  object_type VARCHAR(255),
                                  object_id BIGINT,
                                  created_timestamp BIGINT,
                                  created_by BIGINT,
                                  message_level VARCHAR(128),
                                  message VARCHAR(255),
                                  PRIMARY KEY (id)
);

drop table if exists auth CASCADE;
CREATE TABLE auth (
                      id BIGSERIAL NOT NULL,
                      username VARCHAR(255),
                      email VARCHAR(255),
                      password VARCHAR(128),
                      role VARCHAR(255),
                      forgot_password_key VARCHAR(128),
                      forgot_password_key_timestamp BIGINT,
                      company_unit_id BIGINT,
                      PRIMARY KEY (id)
);

drop table if exists case_ CASCADE;
CREATE TABLE case_ (
                       id BIGSERIAL NOT NULL,
                       case_number VARCHAR(128),
                       case_tome VARCHAR(128),
                       title_ru VARCHAR(128),
                       title_kz VARCHAR(128),
                       title_en VARCHAR(128),
                       start_date BIGINT,
                       finished_date BIGINT,
                       page_number BIGINT,
                       eds_signature_flag BOOLEAN,
                       eds_signature TEXT,
                       sending_naf_flag BOOLEAN,
                       deletion_flag BOOLEAN,
                       limited_access_flag BOOLEAN,
                       hash VARCHAR(128),
                       version INT,
                       id_version VARCHAR(128),
                       active_version_flag BOOLEAN,
                       note VARCHAR(255),
                       id_location BIGINT,
                       id_case_index BIGINT,
                       id_inventory BIGINT,
                       id_destruction_act BIGINT,
                       id_structural_subdivision BIGINT,
                       blockchain_address VARCHAR(128),
                       blockchain_add_date BIGINT,
                       created_timestamp BIGINT,
                       created_by BIGINT,
                       updated_timestamp BIGINT,
                       updated_by BIGINT,
                       PRIMARY KEY (id)
);

drop table if exists case_index CASCADE;
CREATE TABLE case_index (
                            id BIGSERIAL NOT NULL,
                            case_index VARCHAR(128),
                            title_ru VARCHAR(128),
                            title_kz VARCHAR(128),
                            title_en VARCHAR(128),
                            storage_type INT,
                            storage_year INT,
                            note VARCHAR(128),
                            company_unit_id BIGINT,
                            nomenclature_id BIGINT,
                            created_timestamp BIGINT,
                            created_by BIGINT,
                            updated_timestamp BIGINT,
                            updated_by BIGINT,
                            PRIMARY KEY (id)
);

drop table if exists catalog CASCADE;
CREATE TABLE catalog (
                         id BIGSERIAL NOT NULL,
                         name_ru VARCHAR(128),
                         name_kz VARCHAR(128),
                         name_en VARCHAR(128),
                         parent_id BIGINT,
                         company_unit_id BIGINT,
                         created_timestamp BIGINT,
                         created_by BIGINT,
                         updated_timestamp BIGINT,
                         updated_by BIGINT,
                         PRIMARY KEY (id)
);

drop table if exists catalog_case CASCADE;
CREATE TABLE catalog_case (
                              id BIGSERIAL NOT NULL,
                              case_id BIGINT,
                              catalog_id BIGINT,
                              company_unit_id BIGINT,
                              created_timestamp BIGINT,
                              created_by BIGINT,
                              updated_timestamp BIGINT,
                              updated_by BIGINT,
                              PRIMARY KEY (id)
);

drop table if exists company CASCADE;
CREATE TABLE company (
                         id BIGSERIAL NOT NULL,
                         name_ru VARCHAR(128),
                         name_kz VARCHAR(128),
                         name_en VARCHAR(128),
                         bin VARCHAR(32),
                         parent_d BIGINT,
                         fond_id BIGINT,
                         created_timestamp BIGINT,
                         created_by BIGINT,
                         updated_timestamp BIGINT,
                         updated_by BIGINT,
                         PRIMARY KEY (id)
);

drop table if exists company_unit CASCADE;
CREATE TABLE company_unit (
                              id BIGSERIAL NOT NULL,
                              name_ru VARCHAR(128),
                              name_kz VARCHAR(128),
                              name_en VARCHAR(128),
                              parent_id BIGINT,
                              year INT,
                              company_id INT,
                              code_index VARCHAR(16),
                              created_timestamp BIGINT,
                              created_by BIGINT,
                              updated_timestamp BIGINT,
                              updated_by BIGINT,
                              PRIMARY KEY (id)
);

drop table if exists destruction_act CASCADE;
CREATE TABLE destruction_act (
                                 id BIGSERIAL NOT NULL,
                                 act_number VARCHAR(128),
                                 footing VARCHAR(128),
                                 id_structural_subdivision BIGINT,
                                 created_timestamp BIGINT,
                                 created_by BIGINT,
                                 updated_timestamp BIGINT,
                                 updated_by BIGINT,
                                 PRIMARY KEY (id)
);

drop table if exists file CASCADE;
CREATE TABLE file (
                      id BIGSERIAL NOT NULL,
                      name VARCHAR(128),
                      type VARCHAR(128),
                      size BIGINT,
                      page_count INT,
                      hash VARCHAR(128),
                      is_deleted BOOLEAN,
                      file_binary_id BIGINT,
                      created_timestamp BIGINT,
                      created_by BIGINT,
                      updated_timestamp BIGINT,
                      updated_by BIGINT,
                      PRIMARY KEY (id)
);

drop table if exists file_routing CASCADE;
CREATE TABLE file_routing (
                              id BIGSERIAL NOT NULL,
                              file_id BIGINT,
                              table_name VARCHAR(128),
                              table_id BIGINT,
                              type VARCHAR(128),
                              PRIMARY KEY (id)
);

drop table if exists fond CASCADE;
CREATE TABLE fond (
                      id BIGSERIAL NOT NULL,
                      fond_number VARCHAR(128),
                      created_timestamp BIGINT,
                      created_by BIGINT,
                      updated_timestamp BIGINT,
                      updated_by BIGINT,
                      PRIMARY KEY (id)
);

drop table if exists request_status_history CASCADE;
CREATE TABLE request_status_history (
                                        id BIGSERIAL NOT NULL,
                                        request_id BIGINT,
                                        status VARCHAR(64),
                                        created_timestamp BIGINT,
                                        created_by BIGINT,
                                        updated_timestamp BIGINT,
                                        updated_by BIGINT,
                                        PRIMARY KEY (id)
);

drop table if exists location CASCADE;
CREATE TABLE location (
                          id BIGSERIAL NOT NULL,
                          row VARCHAR(64),
                          line VARCHAR(64),
                          column_ VARCHAR(64),
                          box VARCHAR(64),
                          company_unit_id BIGINT,
                          created_timestamp BIGINT,
                          created_by BIGINT,
                          updated_timestamp BIGINT,
                          updated_by BIGINT,
                          PRIMARY KEY (id)
);


drop table if exists nomenclature CASCADE;
CREATE TABLE nomenclature (
                              id BIGSERIAL NOT NULL,
                              nomenclatureNumber VARCHAR(128),
                              year INT,
                              nomenclature_summary_id BIGINT,
                              company_unit_id BIGINT,
                              created_timestamp BIGINT,
                              created_by BIGINT,
                              updated_timestamp BIGINT,
                              updated_by BIGINT,
                              PRIMARY KEY (id)
);

drop table if exists nomenclature_summary CASCADE;
CREATE TABLE nomenclature_summary(
                                     id BIGSERIAL NOT NULL,
                                     number VARCHAR(128),
                                     year INT,
                                     company_unit_id BIGINT,
                                     created_timestamp BIGINT,
                                     created_by BIGINT,
                                     updated_timestamp BIGINT,
                                     updated_by BIGINT,
                                     PRIMARY KEY (id)
);

drop table if exists notification CASCADE;
CREATE TABLE notification (
                              id BIGSERIAL NOT NULL,
                              object_type VARCHAR(128),
                              object_id BIGINT,
                              company_unit_id BIGINT,
                              userId BIGINT,
                              created_timestamp BIGINT,
                              viewed_timestamp BIGINT,
                              is_viewed BOOLEAN,
                              title VARCHAR(255),
                              text VARCHAR(255),
                              company_id BIGINT,
                              PRIMARY KEY (id)
);

drop table if exists record CASCADE;
CREATE TABLE record (
                        id BIGSERIAL NOT NULL,
                        number VARCHAR(128),
                        type VARCHAR(128),
                        company_unit_id BIGINT,
                        created_timestamp BIGINT,
                        created_by BIGINT,
                        updated_timestamp BIGINT,
                        updated_by BIGINT,
                        PRIMARY KEY (id)
);

drop table if exists request CASCADE ;
CREATE TABLE request (
                         id BIGSERIAL NOT NULL,
                         request_user_id BIGINT,
                         response_user_id BIGINT,
                         case_id BIGINT,
                         case_index_id BIGINT,
                         created_type VARCHAR(64),
                         comment VARCHAR(255),
                         status VARCHAR(64),
                         timestamp BIGINT,
                         share_start BIGINT,
                         share_finish BIGINT,
                         favorite BOOLEAN,
                         update_timestamp BIGINT,
                         update_by BIGINT,
                         decline_note VARCHAR(255),
                         company_unit_id BIGINT,
                         from_request_id BIGINT,
                         PRIMARY KEY (id)
);

drop table if exists search_key CASCADE;
CREATE TABLE search_key (
                            id BIGSERIAL NOT NULL,
                            name VARCHAR(128),
                            company_unit_id BIGINT,
                            created_timestamp BIGINT,
                            created_by BIGINT,
                            updated_timestamp BIGINT,
                            updated_by BIGINT,
                            PRIMARY KEY (id)
);

drop table if exists search_key_routing CASCADE;
CREATE TABLE search_key_routing (
                                    id BIGSERIAL NOT NULL,
                                    search_key_id BIGINT,
                                    table_name VARCHAR(128),
                                    table_id BIGINT,
                                    type VARCHAR(128),
                                    PRIMARY KEY (id)
);

drop table if exists share CASCADE;
CREATE TABLE share (
                       id BIGSERIAL NOT NULL,
                       request_id BIGINT,
                       note VARCHAR(255),
                       sender_id BIGINT,
                       receiver_id BIGINT,
                       share_timestamp BIGINT,
                       PRIMARY KEY (id)
);

drop table if exists temp_files CASCADE;
CREATE TABLE temp_files (
                            id BIGSERIAL NOT NULL,
                            file_binary TEXT,
                            file_binary_byte SMALLINT,
                            PRIMARY KEY (id)
);

drop table if exists users CASCADE;
CREATE TABLE users (
                       id BIGSERIAL NOT NULL,
                       auth_id BIGINT,
                       name VARCHAR(128),
                       full_name VARCHAR(128),
                       surname VARCHAR(128),
                       second_name VARCHAR(128),
                       status VARCHAR(128),
                       company_unit_id BIGINT,
                       password VARCHAR(128),
                       last_login_timestamp BIGINT,
                       iin VARCHAR(32),
                       is_active BOOLEAN,
                       is_activated BOOLEAN,
                       created_timestamp BIGINT,
                       created_by BIGINT,
                       updated_timestamp BIGINT,
                       updated_by BIGINT,
                       PRIMARY KEY (id)
);

ALTER TABLE users
    ADD CONSTRAINT fk1 FOREIGN KEY (auth_id) REFERENCES auth(id);

ALTER TABLE company_unit
    ADD CONSTRAINT fk1 FOREIGN KEY (company_id) REFERENCES company(id);

ALTER TABLE company
    ADD CONSTRAINT fk1 FOREIGN KEY (fond_id) REFERENCES fond(id);

ALTER TABLE record
    ADD CONSTRAINT fk1 FOREIGN KEY (company_unit_id) REFERENCES company_unit(id);

ALTER TABLE share
    ADD CONSTRAINT fk1 FOREIGN KEY (request_id) REFERENCES request(id);

ALTER TABLE request_status_history
    ADD CONSTRAINT fk1 FOREIGN KEY (request_id) REFERENCES request(id);

ALTER TABLE catalog_case
    ADD CONSTRAINT fk1 FOREIGN KEY (catalog_id) REFERENCES catalog(id);

ALTER TABLE case_
    ADD CONSTRAINT fk1 FOREIGN KEY (id_location) REFERENCES location(id);

ALTER TABLE catalog_case
    ADD CONSTRAINT fk2 FOREIGN KEY (case_id) REFERENCES case_(id);

ALTER TABLE catalog_case
    ADD CONSTRAINT fk3 FOREIGN KEY (company_unit_id) REFERENCES company_unit(id);

ALTER TABLE file_routing
    ADD CONSTRAINT fk1 FOREIGN KEY (table_id) REFERENCES case_(id);

ALTER TABLE request
    ADD CONSTRAINT fk1 FOREIGN KEY (case_id) REFERENCES case_(id);

ALTER TABLE request
    ADD CONSTRAINT fk2 FOREIGN KEY (case_index_id) REFERENCES case_index(id);

ALTER TABLE file_routing
    ADD CONSTRAINT fk2 FOREIGN KEY (file_id) REFERENCES file(id);

ALTER TABLE file
    ADD CONSTRAINT fk1 FOREIGN KEY (file_binary_id) REFERENCES temp_files(id);

ALTER TABLE case_
    ADD CONSTRAINT fk2 FOREIGN KEY (id_structural_subdivision) REFERENCES company_unit(id);

ALTER TABLE case_
    ADD CONSTRAINT fk3 FOREIGN KEY (id_destruction_act) REFERENCES destruction_act(id);

ALTER TABLE case_
    ADD CONSTRAINT fk4 FOREIGN KEY (id_inventory) REFERENCES record(id);

ALTER TABLE case_
    ADD CONSTRAINT fk5 FOREIGN KEY (id_case_index) REFERENCES case_index(id);

ALTER TABLE nomenclature_summary
    ADD CONSTRAINT fk1 FOREIGN KEY (company_unit_id) REFERENCES company_unit(id);

ALTER TABLE users
    ADD CONSTRAINT fk2 FOREIGN KEY (company_unit_id) REFERENCES company_unit(id);

ALTER TABLE request
    ADD CONSTRAINT fk3 FOREIGN KEY (company_unit_id) REFERENCES company_unit(id);

ALTER TABLE nomenclature
    ADD CONSTRAINT fk1 FOREIGN KEY (company_unit_id) REFERENCES company_unit(id);

ALTER TABLE search_key
    ADD CONSTRAINT fk1 FOREIGN KEY (company_unit_id) REFERENCES company_unit(id);

ALTER TABLE search_key_routing
    ADD CONSTRAINT fk1 FOREIGN KEY (search_key_id) REFERENCES search_key(id);

ALTER TABLE search_key_routing
    ADD CONSTRAINT fk2 FOREIGN KEY (table_id) REFERENCES case_(id);

ALTER TABLE destruction_act
    ADD CONSTRAINT fk1 FOREIGN KEY (id_structural_subdivision) REFERENCES company_unit(id);

ALTER TABLE case_index
    ADD CONSTRAINT fk1 FOREIGN KEY (company_unit_id) REFERENCES company_unit(id);




