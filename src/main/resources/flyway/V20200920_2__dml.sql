insert into activity_journal(even_type, object_type, object_id, created_timestamp, created_by, message_level, message)
values ('1','1',1,1,1,'1','1'),
       ('2','2',2,2,2,'2','2'),
       ('3','3',3,3,3,'3','3'),
       ('4','4',4,4,4,'4','4');
insert into auth(username, email, password, role, forgot_password_key, forgot_password_key_timestamp, company_unit_id)
values ('1','1','1','1',1,1,1),
       ('2','2','2','2',2,2,2),
       ('3','3','3','3',3,3,3);
insert into case_(case_number, case_tome, title_ru, title_kz, title_en, start_date, finished_date, page_number, eds_signature_flag,
                  eds_signature, sending_naf_flag, deletion_flag, limited_access_flag, hash, version, id_version, active_version_flag,
                  note, id_location, id_case_index, id_inventory, id_destruction_act, id_structural_subdivision, blockchain_address,
                  blockchain_add_date, created_timestamp, created_by, updated_timestamp, updated_by)
values ('1','1','1','1','1',1,1,1,true,'1',true,true,true,'1',1,'1',true,'1',1,1,1,1,1,'1',1,1,1,1,1),
       ('2','2','2','2','2',2,2,2,true,'2',true,true,true,'2',2,'2',true,'2',2,2,2,2,2,'2',2,2,2,2,2),
       ('3','3','3','3','3',3,3,3,true,'3',true,true,true,'3',3,'3',true,'3',3,3,3,3,3,'3',3,3,3,3,3);
insert into case_index(case_index, title_ru, title_kz, title_en, storage_type, storage_year, note, company_unit_id, nomenclature_id,
                       created_timestamp, created_by, updated_timestamp, updated_by)
values ('1','1','1','1',1,1,'1',1,1,1,1,1,1),
       ('2','2','2','2',2,2,'2',2,2,2,2,2,2),
       ('3','3','3','3',3,3,'3',3,3,3,3,3,3);
insert into catalog(name_ru, name_kz, name_en, parent_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES ('1','1','1',1,1,1,1,1,1),('2','2','2',2,2,2,2,2,2),('3','3','3',3,3,3,3,3,3);
insert into catalog_case(case_id, catalog_id, company_unit_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES (1,1,1,1,1,1,1),(2,2,2,2,2,2,2),(3,3,3,3,3,3,3);
insert into company(name_ru, name_kz, name_en, bin, parent_d, fond_id, created_timestamp, created_by, updated_timestamp, updated_by)
VALUES ('1','1','1','1',1,1,1,1,1,1),('2','2','2','',2,2,2,2,2,2),('3','3','3','3',3,3,3,3,3,3);